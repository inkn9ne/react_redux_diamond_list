// modified WAI-ARIA 1.1 listbox design pattern

import React, { Component } from 'react'
import PropTypes from 'prop-types'

class DropDown extends Component {
    constructor(props) {
        super(props)
        this.setWrapperRef = this.setWrapperRef.bind(this)       
        this.setListRef = this.setListRef.bind(this)    
        this.setButtonRef = this.setButtonRef.bind(this) 
        this.hideListbox = this.hideListbox.bind(this)
        this.getIndexForItem = this.getIndexForItem.bind(this)  
        this.handleClickOutside = this.handleClickOutside.bind(this)
        this.onSelectItem = this.onSelectItem.bind(this)
        this.onKeyDown = this.onKeyDown.bind(this)
        this.selectedItemIndex = -1
        this.selectedDOMItem
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside)
    }

    componentWillUnmount() {
        document.removeEventListener('mousedown', this.handleClickOutside)
    }

    componentDidUpdate(prevProps) {    
        if (this.props.isOpen ) {
            this.listRef.focus()
            this.selectedDOMItem = this.listRef.querySelector('[aria-selected="true"]')
        }else if(!this.props.isOpen && prevProps.isOpen){
            // we dont need to check for focus after closing the popup with a click outside 
            // of the element, because the buttonref is then not focusable
            this.buttonRef.focus()
        }
    }

    /**
     * Set the DOM refs for needed html elements
     */
    setWrapperRef(node) {
        this.wrapperRef = node
    }

    setListRef(node) {
        this.listRef = node
    }

    setButtonRef(node) {
        this.buttonRef = node
    }

    onSelectItem(item){
        this.props.onChange(item)
    }

    onKeyDown(event){
        // console.log("onKeyDown:event.keyCode = "+event.keyCode)
        
        if(event.keyCode === 13){ // Return key
            event.preventDefault()
            this.props.onToggle(!this.props.isOpen)
        }else if(event.keyCode === 27){ // Esc key
            event.preventDefault()
            this.hideListbox()
        }else if(event.keyCode === 40 && this.props.isOpen){ // Down
            event.preventDefault()
            let nextUnselectedItem = this.selectedDOMItem.nextElementSibling
            if(nextUnselectedItem){
                this.selectedItemIndex = this.getIndexForItem(this.props.selectedItem) + 1 //this.getIndexForItem(this.selectedItem) + 1
                let nextItem = this.props.items[this.selectedItemIndex]
                this.onSelectItem(nextItem)
            }
        }else if(event.keyCode === 38){ // Up
            event.preventDefault()

            let previousUnselectedItem = this.selectedDOMItem.previousElementSibling
            if(previousUnselectedItem){
                this.selectedItemIndex = this.getIndexForItem(this.props.selectedItem) - 1 //this.getIndexForItem(this.selectedItem) + 1
                let previousItem = this.props.items[this.selectedItemIndex]
                this.onSelectItem(previousItem)
            }    
        }
    }

    getIndexForItem (item) {
        return this.props.items.indexOf(item)
    }

    hideListbox () {
        if(this.props.isOpen){
            this.props.onToggle(!this.props.isOpen)
        }
    }
    /**
     * close if clicked outside of element
     */
    handleClickOutside(event) {
        if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
            this.hideListbox()
        }
    }

    render() { 
        return (
            <div className="listbox-area">
                <div className="left-area">
                    <span id={this.props.name+"_exp_elem"}>
                        {this.props.labelText}
                    </span>               
                    <div className={this.props.isOpen?'dropdown-wrapper is-dropdown-open':'dropdown-wrapper'} 
                    ref={this.setWrapperRef} tabIndex='0' onKeyDown ={e => {this.onKeyDown(e)}}>
                        <button className="dropdown-label" aria-haspopup="listbox"
                            aria-labelledby={this.props.name+"_exp_elem exp_button"}
                            aria-expanded={this.props.isOpen?'true':null}
                            id={this.props.name+"_exp_button"} 
                            ref={this.setButtonRef}
                            onClick={e => {
                            this.props.onToggle(!this.props.isOpen)}}>
                                <p>{this.props.selectedItem}</p>
                                <div className={this.props.isOpen?'arrow-up':'arrow-down'}></div>
                        </button>
                        <ul id={this.props.name+"_exp_elem_list"} ref={this.setListRef}
                            tabIndex="-1"
                            role="listbox"
                            aria-labelledby={this.props.name+"_exp_elem"}
                            className="dropdown-list hidden" 
                            aria-activedescendant={
                                this.props.isOpen?this.props.name+'_exp_elem_'+this.props.selectedItem:null
                            }>
                            {this.props.items.map((item, index ) => 
                                <li id={this.props.name+"_exp_elem_"+item} className='dropdown-item'
                                aria-selected = {item === this.props.selectedItem?'true':'false'}
                                key={index} onClick ={e => {this.onSelectItem(item)}} 
                                role="option" tabIndex="-1">
                                    {item}
                                </li>)
                            }
                        </ul>
                    </div>  
                </div>
            </div>
        )
    }
}

DropDown.propTypes = {
    items: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    onChange: PropTypes.func.isRequired,
    onToggle: PropTypes.func.isRequired,
    selectedItem: PropTypes.string.isRequired,
    isOpen: PropTypes.bool.isRequired,
    labelText: PropTypes.string
}

export default DropDown