import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import { withRouter } from 'react-router-dom'
// this also works with react-router-native
import CategoryDropDown from './CategoryDropDown'
import { setVisibilityFilter, setCategoryFilter} from '../actions'

const FilterBar =  withRouter(({history, filterChange, categories, selectedCategoryFilter}) => (
    <div>
        <div className="filter-column">
            <span>Name:</span>
            <input type="text" onChange={e => {
            e.preventDefault()
            filterChange(e.target.value)}} />
        </div>
        <div className="filter-column">
            <CategoryDropDown name="categoryDropper" labelText="Categories:" categoryFilter={selectedCategoryFilter} categoryChange={(filterString) => {history.push('/'+filterString)}}/>
        </div>
    </div>
))

const mapStateToProps = (state,ownProps)  => {
    return {
        labelText: ownProps.labelText,
        categories: state.diamonds.categories?state.diamonds.categories:[],
        layout: state.diamonds.layout,
        visibilityFilter: state.visibilityFilter,
        selectedCategoryFilter: ownProps.categoryFilter 
    }
}
  
const mapDispatchToProps = dispatch => {
    return {
        filterChange: filterString => {
            dispatch(setVisibilityFilter(filterString))
        }
    }
}
  
FilterBar.propTypes = {
    categories: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    filterChange: PropTypes.func.isRequired,
    selectedCategoryFilter: PropTypes.string.isRequired
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(FilterBar)