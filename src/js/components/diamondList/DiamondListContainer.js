import React, { Component } from 'react'
import { render } from 'react-dom'
import {connect} from 'react-redux'
import { diamondsLoaded} from '../../actions'
import '../../../json/clients.json' // this import is used to ensure webpack copies the file to the (dev)server
import DiamondList from './DiamondList'
import FilterBar from '../FilterBar'
import PropTypes from 'prop-types'

class DiamondListContainer extends React.Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        fetch('json/clients.json', 
        {
            headers: {
                'Accept': 'application/json',
            }
        })
        .then( (response) => response.json())
        .then( (response) => ({items: response.items, categories: response.categories}))
        .then( ({items, categories})  => {
            this.props.diamondsLoaded(items,categories)
        })
        .catch(function(error) {
            console.log("DiamondListContainer:: error loading items.json" + error)
        })
    }
      
    render() {
        return (
            <div>
                <h3>Companies</h3>
                <FilterBar categoryFilter={this.props.categoryFilter}/>
                <hr width='100%'/>
                <DiamondList categoryFilter={this.props.categoryFilter}/>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        diamondsLoaded: (items, categories) => {
            dispatch(diamondsLoaded(items, categories))
        }
    }
}

DiamondListContainer.propTypes = {
    diamondsLoaded: PropTypes.func.isRequired
}

export default connect(
    null,
    mapDispatchToProps
)(DiamondListContainer)