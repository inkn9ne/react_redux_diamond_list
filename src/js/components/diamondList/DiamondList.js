import React, { Component } from 'react'
import { render } from 'react-dom'
import {connect} from 'react-redux'
import DiamondLayout from './DiamondLayout'
import { changeDiamondLayout, setVisibilityFilter } from '../../actions'
import PropTypes from 'prop-types'

const SCREENSIZE_L = "L"
const SCREENSIZE_M = "M"
const SCREENSIZE_S = "S"

class DiamondList extends React.Component {
  constructor(props) {
    super(props)
    this.itemWidth = 118
    this.currentScreenSize = "not defined"
    this.layouter = new DiamondLayout(this.itemWidth,7)
    this._fullDebug = false
  }

  componentDidMount() {
    window.addEventListener("resize",  (e) => {
      this.checkScreenSize()
    })
    this.checkScreenSize()
  }

  componentWillReceiveProps(nextProps) {
    if(this._fullDebug)console.log("DiamondList:: componentWillReceiveProps:: nextProps = ")
    if(this._fullDebug)console.log(nextProps)
    if (this.currentScreenSize !== nextProps.layout) {
      if(this._fullDebug)console.log("nextProps.layout = "+nextProps.layout)
      this.currentScreenSize == nextProps.layout
      this.layouter.changeScreenSize(nextProps.layout)
    }
  }
  
  componentWillUnmount() {
    window.removeEventListener("resize",  (e) => {
      this.checkScreenSize()
    })     
  }

  checkScreenSize () {
    let newScreenSize
    if(window.innerWidth >= 992){
      newScreenSize = SCREENSIZE_L
    }else if(window.innerWidth < 992 && window.innerWidth >= 744){
      newScreenSize = SCREENSIZE_M
    }else{
      newScreenSize = SCREENSIZE_S
    }
    if(newScreenSize != this.currentScreenSize){
      this.props.changeDiamondLayout(newScreenSize)
    }
  } 

  render() {
    if(this._fullDebug)console.log("DiamondList:: render:: this.props.diamonds = ")
    if(this._fullDebug)console.log(this.props)
    if(this.props.items && this.props.items.length > 0){
      let items = this.props.items
      if(this.props.visibilityFilter){
        items = items.filter (item => 
          item.name.toLowerCase()
          .includes(this.props.visibilityFilter.toLowerCase())
        )
      }
      if(this.props.categoryFilter && this.props.categoryFilter!=="all"){
        items = items.filter (item => 
          item.category.toLowerCase().includes(this.props.categoryFilter.toLowerCase())
        )
      }
      return (
        <div className = "diamond-list-container">
            <ul className="diamond-list" style={{height:this.layouter.getContentHeight(items.length)}}>
            {items.map((item, index )=> 
              <li key={item.id}>
                <div className = 'diamond-item'  
                style={{opacity: 1, 
                left:this.layouter.getItemPosition(index).x, top:this.layouter.getItemPosition(index).y}}>
                  <div className = 'content'>
                    <img src={item.image} style={{width:this.itemWidth, height:this.itemWidth}}/>
                    {item.name}
                  </div>
                </div>
              </li>)}
            </ul>    
        </div>
      )
    }else{
      return (<div>no items</div>)
    }
  }
}
const mapStateToProps = (state,ownProps)  => {
  return {
    items: state.diamonds.items?state.diamonds.items:[],
    layout: state.diamonds.layout,
    visibilityFilter: state.visibilityFilter,
    categoryFilter: ownProps.categoryFilter   
  }
}

const mapDispatchToProps = dispatch => {
  return {
    changeDiamondLayout: layout => {
      dispatch(changeDiamondLayout(layout))
    }
  }
}

DiamondList.propTypes = {
  diamonds: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
  changeDiamondLayout: PropTypes.func.isRequired
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DiamondList)