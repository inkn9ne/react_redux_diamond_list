const SCREENSIZE_L = "L"
const SCREENSIZE_M = "M"
const SCREENSIZE_S = "S"

export default class DiamondLayout {
    constructor(itemWidth, itemSpacing, currentScreenSize) {
        this.rowsArrayL  = 
            [{z: 1,p: 1},{z: 1,p: 3},{z: 1,p: 5},{z: 1,p: 7},{z: 2,p: 1},
            {z: 2,p: 2},{z: 2,p: 3},{z: 2,p: 4},{z: 2,p: 5},{z: 2,p: 6},
            {z: 2,p: 7},{z: 2,p: 8},{z: 3,p: 1},{z: 3,p: 2},{z: 3,p: 3},
            {z: 3,p: 4},{z: 3,p: 5},{z: 3,p: 6},{z: 3,p: 7},{z: 4,p: 2},
            {z: 4,p: 3},{z: 4,p: 4},{z: 4,p: 5},{z: 4,p: 6},{z: 4,p: 7},
            {z: 5,p: 2},{z: 5,p: 3},{z: 5,p: 4},{z: 5,p: 6},{z: 6,p: 3},
            {z: 6,p: 4},{z: 7,p: 3},{z: 7,p: 4},{z: 7,p: 5},{z: 7,p: 6},
            {z: 8,p: 1},{z: 8,p: 2},{z: 8,p: 3},{z: 9,p: 1},{z: 9,p: 2},
            {z: 9,p: 3},{z: 9,p: 4}]
        this.rowsArrayM = 
            [{z: 1,p: 2},{z: 1,p: 4},{z: 2,p: 2},{z: 2,p: 3},{z: 2,p: 4},
            {z: 2,p: 5},{z: 3,p: 1},{z: 3,p: 2},{z: 3,p: 3},{z: 3,p: 4},
            {z: 3,p: 5},{z: 4,p: 1},{z: 4,p: 2},{z: 4,p: 3},{z: 4,p: 4},
            {z: 4,p: 5},{z: 4,p: 6},{z: 5,p: 1},{z: 5,p: 2},{z: 5,p: 3},
            {z: 5,p: 4},{z: 5,p: 5},{z: 6,p: 2},{z: 6,p: 3},{z: 6,p: 4},
            {z: 6,p: 5},{z: 7,p: 2},{z: 7,p: 3},{z: 7,p: 4},{z: 8,p: 3},
            {z: 8,p: 4},{z: 9,p: 3},{z: 9,p: 4},{z: 10,p: 1},{z: 10,p: 2},
            {z: 11,p: 1},{z: 11,p: 2},{z: 11,p: 3},{z: 12,p: 1},{z: 12,p: 2},
            {z: 13,p: 1},{z: 13,p: 2},{z: 13,p: 3},{z: 14,p: 1}]
        this.rowsArrayS = 
            [{z: 1,p: 1},{z: 1,p: 3},{z: 2,p: 1},{z: 2,p: 2},{z: 2,p: 3},
            {z: 2,p: 4},{z: 3,p: 1},{z: 3,p: 2},{z: 3,p: 3},{z: 4,p: 2},
            {z: 4,p: 3},{z: 5,p: 1},{z: 5,p: 2},{z: 5,p: 3},{z: 6,p: 1},
            {z: 6,p: 2},{z: 6,p: 3},{z: 6,p: 4},{z: 7,p: 1},{z: 7,p: 2},
            {z: 7,p: 3},{z: 8,p: 2},{z: 8,p: 3},{z: 9,p: 1},{z: 9,p: 2},
            {z: 9,p: 3},{z: 10,p: 1},{z: 10,p: 2},{z: 10,p: 3},{z: 10,p: 4},
            {z: 11,p: 1},{z: 11,p: 3},{z: 12,p: 1},{z: 13,p: 1},{z: 13,p: 2},
            {z: 14,p: 1},{z: 15,p: 1},{z: 15,p: 2},{z: 16,p: 1},{z: 17,p: 1},
            {z: 17,p: 2},{z: 18,p: 1},{z: 19,p: 1},{z: 19,p: 2},{z: 20,p: 1},
            {z: 21,p: 1},{z: 21,p: 2},{z: 22,p: 1},{z: 23,p: 1},{z: 23,p: 2}]
  
        this.currentRowsArray = this.rowsArrayL
        this.currentScreenSize = currentScreenSize
        this.itemWidth = itemWidth
        this.scaledIconWidth = itemWidth * (1 / Math.sqrt(2))
        this.itemSpacing = itemSpacing
        this.useXoffsetL = true
        this.useXoffsetM = true
        this.useXoffsetS = true
        this.contentHeight = 100
        this.maxHeight = 0
        this._fullDebug = false
    }
    
    changeScreenSize (newScreenSize) {
        this.maxHeight = 0
        this.currentScreenSize = newScreenSize
        if(this.currentScreenSize == SCREENSIZE_L){
            this.currentRowsArray = this.rowsArrayL
        }else if(this.currentScreenSize == SCREENSIZE_M){
            this.currentRowsArray = this.rowsArrayM
        }else{
            this.currentRowsArray = this.rowsArrayS
        }
    }

    getContentHeight (elementsLength) {
        if(this._fullDebug)console.log("DiamondLayout:: getContentHeight:: elementsLength = " + elementsLength)
        let contentHeight = 0
        for(var i = 0; i < elementsLength; i++){
            let currentRowIndex = this.currentRowsArray[i].z
            let yVal = (this.itemWidth - this.scaledIconWidth) / 2 + ((currentRowIndex  - 1) 
            * (this.itemWidth + this.itemSpacing) / 2)
            let itemHeight = this.scaledIconWidth
            let newHeight = yVal + itemHeight +  + this.itemSpacing
            if(newHeight > contentHeight) {
                contentHeight = newHeight
            }
        }
        if(this._fullDebug)console.log("DiamondLayout:: getContentHeight:: contentHeight = " + contentHeight)
        return Math.round(contentHeight)
    }

    getItemPosition (i) {
        let currentRowIndex = this.currentRowsArray[i].z
        let currentPlace = this.currentRowsArray[i].p
        let xVal = (this.itemWidth - this.scaledIconWidth) / 2 
        + ((currentPlace  - 1 ) * (this.itemWidth + this.itemSpacing))
        let yVal = (this.itemWidth - this.scaledIconWidth) / 2 
        + ((currentRowIndex  - 1) * (this.itemWidth + this.itemSpacing) / 2)
        xVal = this.checkXOffset(xVal, this.currentRowsArray[i])
        let itemHeight = this.scaledIconWidth
        let contentHeight = yVal + itemHeight
        if(contentHeight != this.maxHeight) {
            this.maxHeight = contentHeight
        }
        return {'x': xVal, 'y': yVal}
    }

    checkXOffset (xVal, config) {
        if(this.currentScreenSize == SCREENSIZE_L){
            if(this.useXoffsetL == true){
                if(config.z % 2 == 1){ // its an odd row
                    xVal += ((this.itemSpacing + this.itemWidth) / 2)
                }
            }else{
                if(config.z % 2 == 0){ // its an even row
                    xVal += ((this.itemSpacing + this.itemWidth) / 2)
                }
            }
        }else if(this.currentScreenSize == SCREENSIZE_M){
            if(this.useXoffsetM == true){
                if(config.z % 2 == 1){ // its an odd row
                    xVal += ((this.itemSpacing + this.itemWidth) / 2)
                }
            }else{
                if(config.z % 2 == 0){ // its an even row
                    xVal += ((this.itemSpacing + this.itemWidth) / 2)
                }
            }
        }else if(this.currentScreenSize == SCREENSIZE_S){
            if(this.useXoffsetS == true){
                if(config.z % 2 == 1){ // its an odd row
                    xVal += ((this.itemSpacing + this.itemWidth) / 2)
                }
            }else{
                if(config.z % 2 == 0){ // its an even row
                    xVal += ((this.itemSpacing + this.itemWidth) / 2)
                }
            }
        }
        return xVal
    }
}