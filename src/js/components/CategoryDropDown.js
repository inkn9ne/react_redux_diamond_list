import React from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import { setCategoryFilter, toggleCategoryDropDown} from '../actions'
import DropDown from './DropDown'

const CategoryDropDown =  () => (
    <DropDown labelText={props.labelText}/>
)

CategoryDropDown.propTypes = {
    items: PropTypes.oneOfType([PropTypes.object, PropTypes.array]),
    onChange: PropTypes.func.isRequired,
    selectedItem: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    labelText: PropTypes.string
}

const mapStateToProps = (state, ownProps)  => {
    return {
        labelText:ownProps.labelText,
        name: ownProps.name?ownProps.name:'dropDown',
        items: state.diamonds.categories?state.diamonds.categories:[],
        selectedItem: ownProps.categoryFilter?ownProps.categoryFilter:'all', 
        isOpen:state.categoryFilter.isDropDownOpen,
        isFocused:state.categoryFilter.isFocused
    }
}
  
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onChange: filterString => {
            ownProps.categoryChange(filterString)
        },
        onToggle: isOpen => {
            dispatch(toggleCategoryDropDown(isOpen))
        }
    }
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(DropDown)