import '../../css/main.scss'
import React, { Component } from 'react'
import { render } from 'react-dom'
import DiamondListContainer from './diamondList/DiamondListContainer.js'
import * as actionTypes from '../actionTypes'

const initialState = {
    visibilityFilter: actionTypes.SHOW_ALL,
    diamondItems: [],
    diamondLayout: 'S'
}


export default class App extends Component {
    constructor(props){
        super(props)
        this.onResize = this.onResize.bind(this)
        //this.state = { size: {'width': 1000, 'height': 500 }}
        this._fullDebug = false
    }
   
    componentDidMount() {
        window.addEventListener('resize', this.onResize, false)
        this.onResize()
    }
   
    onResize() {
      if(this._fullDebug)console.log("app: onResize:: window.innerWidth = "+window.innerWidth)
      //this.setState({size: { 'width': window.innerWidth,'height': (window.innerHeight ) }})
    }
   

    render() {
        console.log('this.props.match')
        console.log(this.props.match)
        if(this._fullDebug)console.log("app: render: this.state.size.width = "+this.state.size.width)
        return (
            <div>     
                <section className="diamonds">
                    <article className = 'l-center-margins'>
                      <DiamondListContainer categoryFilter={this.props.match.params.categoryFilter?this.props.match.params.categoryFilter:'all'}/>
                    </article>
                </section>
            </div>
        )
    }
}