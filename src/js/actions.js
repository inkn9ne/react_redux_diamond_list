import * as actionTypes from './actionTypes'
/*
 * action creators
 */

export function diamondsLoaded(items,categories) {
    return { type: actionTypes.DIAMONDS_LOADED,items,categories }
}

export function changeDiamondLayout(layout) {
    return { type: actionTypes.CHANGE_DIAMOND_LAYOUT, layout }
}

export function setVisibilityFilter(filter) {
    return { type: actionTypes.SET_LIST_VISIBILITY_FILTER, filter }
}

export function setCategoryFilter(filter) {
    return { type: actionTypes.SET_LIST_CATEGORY_FILTER, filter }
}

export function toggleCategoryDropDown(isDropDownOpen, isFocused = false) {
    return { type: actionTypes.TOGGLE_CATEGORY_DROPDOWN, isDropDownOpen, isFocused }
}