import { combineReducers } from 'redux'
import diamonds from './diamonds'
import visibilityFilter from './visibilityFilter'
import categoryFilter from './categoryFilter'

const diamondApp = combineReducers({
  diamonds,
  visibilityFilter,
  categoryFilter
})

export default diamondApp