const visibilityFilter = (state = '', action) => {
    switch (action.type) {
      case 'SET_LIST_VISIBILITY_FILTER':
        return action.filter
      default:
        return state
    }
  }
  
  export default visibilityFilter