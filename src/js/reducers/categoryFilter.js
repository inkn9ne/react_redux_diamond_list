const initialState =  {isDropDownOpen: false, isFocused: false, selectedCategoryFilter: 'all' }
const categoryFilter = (state = initialState, action) => {
    switch (action.type) {  
      case 'SET_LIST_CATEGORY_FILTER':
      //not used because selectdCategoryFilter comes from React Router
        return Object.assign({},state, {selectedCategoryFilter: action.filter})
      case 'TOGGLE_CATEGORY_DROPDOWN':
        return  Object.assign({},state, {isDropDownOpen: action.isDropDownOpen})
      default:
        return state
    }
  }
  
  export default categoryFilter