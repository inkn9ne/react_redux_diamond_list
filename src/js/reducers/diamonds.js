const initialState =  {items: [], categories: [],selectedCategoryFilter:"",layout:"M" }
const diamonds = (state = initialState, action) => {
  switch (action.type) {
    case 'DIAMONDS_LOADED':
      return Object.assign({},state, {items: action.items, categories: action.categories, selectedCategoryFilter:action.ca})
      
    case 'CHANGE_DIAMOND_LAYOUT':
      return Object.assign({},state, {layout: action.layout})
    default:
      return state
  }
}

export default diamonds