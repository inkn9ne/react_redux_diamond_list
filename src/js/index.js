import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import diamondApp from './reducers/index.js'
import Root from './components/Root'

let store = createStore( diamondApp)

render(
  <Root store={store} />,
  document.getElementById('root')
)