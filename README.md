# React Redux Diamond List #

Demo application with React and Redux. 

A Json list is loaded and displayed as diamonds in a configured pattern.
There are 3 layout sizes where different patterns are shown. Resize your browser window to change between the layout sizes.
The list can be filtered on categories and a search string.
Filter changes dispatch from the FilterBar to the Redux store, and the DiamondList updates accordingly.
React Router is used to update the url and enable deeplinking.

The category Dropdown implements the WAI-ARIA 1.1 listbox design pattern to 
improve accesibility.

![Scheme](src/img/diamondList.png)

### What is this repository for? ###

* This is an example application with React and Redux.
* The Webpack configuration features Babel es2015 presets and also precompiles Scss files, has a dev-server configuration which recompiles if files are changed, and also a production build configuration with some optimization features.
* Version 1.0.

### How do I get set up? ###

* clone or download the repo
* make sure you have npm installed
* open console and cd to repo directory
* run `npm install`
* to run project on the local dev server run `npm run build`
* for production version run `npm run build:prod`